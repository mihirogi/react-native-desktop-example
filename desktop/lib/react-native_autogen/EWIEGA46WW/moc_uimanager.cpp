/****************************************************************************
** Meta object code from reading C++ file 'uimanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../node_modules/react-native/ReactQt/runtime/src/uimanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'uimanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_UIManager_t {
    QByteArrayData data[44];
    char stringdata0[607];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UIManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UIManager_t qt_meta_stringdata_UIManager = {
    {
QT_MOC_LITERAL(0, 0, 9), // "UIManager"
QT_MOC_LITERAL(1, 10, 33), // "removeSubviewsFromContainerWi..."
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 17), // "containerReactTag"
QT_MOC_LITERAL(4, 63, 7), // "measure"
QT_MOC_LITERAL(5, 71, 8), // "reactTag"
QT_MOC_LITERAL(6, 80, 34), // "ModuleInterface::ListArgument..."
QT_MOC_LITERAL(7, 115, 8), // "callback"
QT_MOC_LITERAL(8, 124, 10), // "updateView"
QT_MOC_LITERAL(9, 135, 8), // "viewName"
QT_MOC_LITERAL(10, 144, 10), // "properties"
QT_MOC_LITERAL(11, 155, 14), // "manageChildren"
QT_MOC_LITERAL(12, 170, 10), // "QList<int>"
QT_MOC_LITERAL(13, 181, 16), // "moveFromIndicies"
QT_MOC_LITERAL(14, 198, 13), // "moveToIndices"
QT_MOC_LITERAL(15, 212, 17), // "addChildReactTags"
QT_MOC_LITERAL(16, 230, 12), // "addAtIndices"
QT_MOC_LITERAL(17, 243, 15), // "removeAtIndices"
QT_MOC_LITERAL(18, 259, 11), // "setChildren"
QT_MOC_LITERAL(19, 271, 12), // "childrenTags"
QT_MOC_LITERAL(20, 284, 26), // "replaceExistingNonRootView"
QT_MOC_LITERAL(21, 311, 11), // "newReactTag"
QT_MOC_LITERAL(22, 323, 13), // "measureLayout"
QT_MOC_LITERAL(23, 337, 16), // "ancestorReactTag"
QT_MOC_LITERAL(24, 354, 13), // "errorCallback"
QT_MOC_LITERAL(25, 368, 29), // "measureLayoutRelativeToParent"
QT_MOC_LITERAL(26, 398, 4), // "blur"
QT_MOC_LITERAL(27, 403, 14), // "setJSResponder"
QT_MOC_LITERAL(28, 418, 20), // "blockNativeResponder"
QT_MOC_LITERAL(29, 439, 16), // "clearJSResponder"
QT_MOC_LITERAL(30, 456, 10), // "createView"
QT_MOC_LITERAL(31, 467, 7), // "rootTag"
QT_MOC_LITERAL(32, 475, 5), // "props"
QT_MOC_LITERAL(33, 481, 13), // "findSubviewIn"
QT_MOC_LITERAL(34, 495, 5), // "point"
QT_MOC_LITERAL(35, 501, 26), // "dispatchViewManagerCommand"
QT_MOC_LITERAL(36, 528, 9), // "commandID"
QT_MOC_LITERAL(37, 538, 11), // "commandArgs"
QT_MOC_LITERAL(38, 550, 12), // "takeSnapshot"
QT_MOC_LITERAL(39, 563, 13), // "REACT_PROMISE"
QT_MOC_LITERAL(40, 577, 6), // "target"
QT_MOC_LITERAL(41, 584, 7), // "options"
QT_MOC_LITERAL(42, 592, 7), // "resolve"
QT_MOC_LITERAL(43, 600, 6) // "reject"

    },
    "UIManager\0removeSubviewsFromContainerWithID\0"
    "\0containerReactTag\0measure\0reactTag\0"
    "ModuleInterface::ListArgumentBlock\0"
    "callback\0updateView\0viewName\0properties\0"
    "manageChildren\0QList<int>\0moveFromIndicies\0"
    "moveToIndices\0addChildReactTags\0"
    "addAtIndices\0removeAtIndices\0setChildren\0"
    "childrenTags\0replaceExistingNonRootView\0"
    "newReactTag\0measureLayout\0ancestorReactTag\0"
    "errorCallback\0measureLayoutRelativeToParent\0"
    "blur\0setJSResponder\0blockNativeResponder\0"
    "clearJSResponder\0createView\0rootTag\0"
    "props\0findSubviewIn\0point\0"
    "dispatchViewManagerCommand\0commandID\0"
    "commandArgs\0takeSnapshot\0REACT_PROMISE\0"
    "target\0options\0resolve\0reject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UIManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x00 /* Private */,
       4,    2,   92,    2, 0x00 /* Private */,
       8,    3,   97,    2, 0x00 /* Private */,
      11,    6,  104,    2, 0x00 /* Private */,
      18,    2,  117,    2, 0x00 /* Private */,
      20,    2,  122,    2, 0x00 /* Private */,
      22,    4,  127,    2, 0x00 /* Private */,
      25,    3,  136,    2, 0x00 /* Private */,
      26,    1,  143,    2, 0x00 /* Private */,
      27,    2,  146,    2, 0x00 /* Private */,
      29,    0,  151,    2, 0x00 /* Private */,
      30,    4,  152,    2, 0x00 /* Private */,
      33,    3,  161,    2, 0x00 /* Private */,
      35,    3,  168,    2, 0x00 /* Private */,
      38,    4,  175,   39, 0x00 /* Private */,

 // methods: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6,    5,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QVariantMap,    5,    9,   10,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 12, 0x80000000 | 12, 0x80000000 | 12, 0x80000000 | 12, 0x80000000 | 12,    3,   13,   14,   15,   16,   17,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 12,    3,   19,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    5,   21,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, 0x80000000 | 6, 0x80000000 | 6,    5,   23,   24,    7,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 6, 0x80000000 | 6,    5,   24,    7,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,    5,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::Int, QMetaType::QVariantMap,    5,    9,   31,   32,
    QMetaType::Void, QMetaType::Int, QMetaType::QPointF, 0x80000000 | 6,    5,   34,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QVariantList,    5,   36,   37,
    QMetaType::Void, QMetaType::QString, QMetaType::QVariantMap, 0x80000000 | 6, 0x80000000 | 6,   40,   41,   42,   43,

       0        // eod
};

void UIManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UIManager *_t = static_cast<UIManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->removeSubviewsFromContainerWithID((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->measure((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[2]))); break;
        case 2: _t->updateView((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QVariantMap(*)>(_a[3]))); break;
        case 3: _t->manageChildren((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QList<int>(*)>(_a[2])),(*reinterpret_cast< const QList<int>(*)>(_a[3])),(*reinterpret_cast< const QList<int>(*)>(_a[4])),(*reinterpret_cast< const QList<int>(*)>(_a[5])),(*reinterpret_cast< const QList<int>(*)>(_a[6]))); break;
        case 4: _t->setChildren((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QList<int>(*)>(_a[2]))); break;
        case 5: _t->replaceExistingNonRootView((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->measureLayout((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[3])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[4]))); break;
        case 7: _t->measureLayoutRelativeToParent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[2])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[3]))); break;
        case 8: _t->blur((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setJSResponder((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 10: _t->clearJSResponder(); break;
        case 11: _t->createView((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QVariantMap(*)>(_a[4]))); break;
        case 12: _t->findSubviewIn((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[3]))); break;
        case 13: _t->dispatchViewManagerCommand((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QVariantList(*)>(_a[3]))); break;
        case 14: _t->takeSnapshot((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[3])),(*reinterpret_cast< const ModuleInterface::ListArgumentBlock(*)>(_a[4]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ModuleInterface::ListArgumentBlock >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 5:
            case 4:
            case 3:
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<int> >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<int> >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ModuleInterface::ListArgumentBlock >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ModuleInterface::ListArgumentBlock >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ModuleInterface::ListArgumentBlock >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ModuleInterface::ListArgumentBlock >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject UIManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_UIManager.data,
      qt_meta_data_UIManager,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *UIManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UIManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_UIManager.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "ModuleInterface"))
        return static_cast< ModuleInterface*>(this);
    if (!strcmp(_clname, "com.canonical.ReactNative.ModuleInterface"))
        return static_cast< ModuleInterface*>(this);
    return QObject::qt_metacast(_clname);
}

int UIManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
