## 概要

react-nativeでデスクトップアプリを作ってみる。  
チュートリアルがないので、適当に触る

https://github.com/status-im/react-native-desktop

### 設定

導入するもの:  
https://github.com/status-im/react-native-desktop/blob/master/docs/InstallPrerequisites.md


`CMAKE_PREFIX_PATH`に`qtライブラリ`へのパスを環境変数として設定する  
例:  
`export CMAKE_PREFIX_PATH=/usr/local/Cellar/qt/5.11.1/lib/cmake/`  

以下のようなメッセージがでたら、パスがちゃんと設定できてない  
```
 Add the installation prefix of "Qt5Core" to CMAKE_PREFIX_PATH or set "Qt5Core_DIR" to a directory containing one of the above files.  If
  "Qt5Core" provides a separate development package or SDK, be sure it has been installed.
```


参考:  
https://stackoverflow.com/questions/38746635/how-to-solve-qt5-packages-not-found-cmake-errors-in-mac  


### 動かし方

公式:  
https://github.com/status-im/react-native-desktop/blob/master/docs/CreateNewApp.md